from rlberry.manager import AgentManager, read_writer_data
from scipy.stats import norm

from sequential_test import GGST, UBTGST, Generator, Mixture
import pandas as pd
import numpy as np


env_ctor = Generator

n_fit = 20
K = None
power = 0.95
alpha = 0.05
df = pd.DataFrame()
n = 5  # size of a group
name = "PK"
student_approx = False

agent_kwargs = {
    "K": K,
    "name": name,
    "alpha": alpha,
    "student_approx": student_approx,
    "sigma": 2,
    "power": power,
    "n":n
}

# GGST
agent = GGST(env_ctor(n=n), **agent_kwargs)  # trigger the boundary pre-computation
print(agent.K)
agent_kwargs['K']=agent.K

agent_kwargs["boundary"] = agent.boundary

for delta in [0, 0.5, 1, 2]:
    env_kwargs = {"laws": [norm(loc=0, scale=2), norm(loc=delta, scale=2)], "n": n}

    ggst_stats = AgentManager(
        GGST,
        (env_ctor, env_kwargs),
        fit_budget=0,
        init_kwargs=agent_kwargs,
        n_fit=n_fit,
        parallelization="process",
        mp_context="fork",
    )
    ggst_stats.fit()
    df2 = read_writer_data(ggst_stats, "decision")
    decisions = df2.loc[df2["tag"] == "decision"]["value"].values
    n_iters = (
        2 * df2.loc[df2["tag"] == "n_iter"]["value"].values
    )  # number of sample used.

    df = pd.concat(
        [
            df,
            pd.DataFrame(
                {
                    "mean_n_iter": [np.mean(n_iters)],
                    "sd_iter": [np.std(n_iters)],
                    "pr_reject": [np.mean(decisions == "reject")],
                    "delta": [delta],
                    "algo":["GGST"],
                }
            ),
        ],
        ignore_index=True,
    )
    ggst_stats.clear_output_dir()

## UBTGST
agent_kwargs = {"alpha": alpha, "power":power}
for delta in [0, 0.5, 1, 2]:
    env_kwargs = {"laws": [norm(loc=0, scale=2), norm(loc=delta, scale=2)], "n": n}

    ggst_stats = AgentManager(
        UBTGST,
        (env_ctor, env_kwargs),
        fit_budget=0,
        init_kwargs=agent_kwargs,
        n_fit=n_fit,
        parallelization="process",
        mp_context="fork",
    )
    ggst_stats.fit()
    df2 = read_writer_data(ggst_stats, "decision")
    decisions = df2.loc[df2["tag"] == "decision"]["value"].values
    n_iters = (
        2 * df2.loc[df2["tag"] == "n_iter"]["value"].values
    )  # number of sample used.

    df = pd.concat(
        [
            df,
            pd.DataFrame(
                {
                    "mean_n_iter": [np.mean(n_iters)],
                    "sd_iter": [np.std(n_iters)],
                    "pr_reject": [np.mean(decisions == "reject")],
                    "delta": [delta],
                    "algo":["UBTGST"],
                }
            ),
        ],
        ignore_index=True,
    )
    ggst_stats.clear_output_dir()


print(df)
df.to_csv(
    "outputs/"
    + name
    + "_student_K_"
    + str(K)
    + "_n_"
    + str(n)
    + "_alpha_"
    + str(alpha)
    + ".csv"
)
