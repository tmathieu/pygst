from rlberry.manager import AgentManager, read_writer_data
from scipy.stats import norm
from scipy import stats

from sequential_test import PTGGST, Generator, Mixture
import pandas as pd
import numpy as np
import gc

env_ctor = Generator

n_fit = 5000
K = 5
alpha = 0.05
df = pd.DataFrame()
n = 5  # size of a group
name = "PK"
student_approx = True

agent_kwargs = {
            "K": K,
            "name": name,
            "alpha": alpha,
            "student_approx": student_approx,
            "sigma": 2, "Kc":2, "B":500}

agent = PTGGST(env_ctor(), **agent_kwargs) # trigger the boundary pre-computation

for delta in [0, 0.5, 1, 1.5]:
    env_kwargs = {"laws": [Mixture(means=np.array([1,6])), Mixture(means=np.array([1+2*delta,6]))], "n": n}
    ggst_stats = AgentManager(
        PTGGST,
        (env_ctor, env_kwargs),
        fit_budget=0,
        init_kwargs=agent_kwargs,
        n_fit=n_fit,
        parallelization="process",
        mp_context="fork",
    )
    ggst_stats.fit()
    df2 = read_writer_data(ggst_stats, "decision")
    decisions = df2.loc[df2["tag"] == "decision"]["value"].values
    n_iters = (
        2 * df2.loc[df2["tag"] == "n_iter"]["value"].values
    )  # number of sample used.
    
    df = pd.concat(
        [
            df,
            pd.DataFrame(
                {
                    "mean_n_iter": [np.mean(n_iters)],
                    "sd_iter":[np.std(n_iters)],
                    "pr_reject": [np.mean(decisions == "reject")],
                    "delta": [delta],
                }
            ),
        ],
        ignore_index=True,
    )
    ggst_stats.clear_output_dir()
    gc.collect()

df = df.set_index(["delta"])
print(df)
df.to_csv("outputs/ptggst_B500_Kc2_"+name+"_mixture_K_"+str(K)+"_n_"+str(n)+"_alpha_"+str(alpha)+".csv")
