from rlberry.manager import AgentManager, read_writer_data
from rlberry.agents.torch import A2CAgent
from rlberry.envs import gym_make
from rlberry.agents.torch.utils.training import model_factory_from_env

from scipy.stats import norm
from sequential_test import GGST, Generator
import pandas as pd
import numpy as np

# GST definition

K = 10 # at most 10 interim
alpha = 0.1
df = pd.DataFrame()
n = 8  # size of a group
name = "OF"
student_approx = True

test_kwargs = {
    "K": K,
    "n": n,
    "name": name,
    "alpha": alpha,
    "student_approx": student_approx,
    "sigma": None,
}

test = GGST(None, **test_kwargs)

# DeepRL agent definition
#from rlberry.wrappers import RescaleRewardWrapper
#env_ctor = RescaleRewardWrapper
#env_kwargs = dict(env = gym_make(id="Acrobot-v1"), reward_range = (0,1))
env_ctor = gym_make
env_kwargs = dict(id = "CartPole-v1")
seed = 42
budget = 1e4


def get_rewards(manager):
    writer_data = manager.get_writer_data()
    return [
        np.sum(
            writer_data[idx].loc[writer_data[idx]["tag"] == "episode_rewards", "value"]
        )
        for idx in writer_data
    ]


# Early stopping test. Impact of batch size
# Typically, rejects the null after 3 groups = 24 fits.

X = np.array([])
Y = np.array([])
for k in range(K):
    manager1 = AgentManager(
        A2CAgent,
        (env_ctor, env_kwargs),
        fit_budget=budget,
        seed=seed,
        eval_kwargs=dict(eval_horizon=500),
	init_kwargs=dict(learning_rate=1e-3,  # Size of the policy gradient
			 entr_coef = 0.0), 
        n_fit=n,
        parallelization="process",
        mp_context="fork",
    )
    manager2 = AgentManager(
        A2CAgent,
        (env_ctor, env_kwargs),
        fit_budget=budget,
        seed=seed,
        init_kwargs=dict(
            learning_rate=1e-3,  # Size of the policy gradient
	    entr_coef = 0.0,  
	    batch_size = 1024 
        ),
        n_fit=n,
        eval_kwargs=dict(eval_horizon=500),
        agent_name="A2C_tuned",
        parallelization="process",
        mp_context="fork",
    )
    manager1.fit()
    manager2.fit()
    X = np.hstack([X, get_rewards(manager1)])
    Y = np.hstack([Y, get_rewards(manager2)])
    test.partial_fit(X, Y)
    manager1.clear_output_dir()
    manager2.clear_output_dir()
    if test.decision == "reject":
        print("Reject the null after " + str(k+1) + " groups")
        break
    else:
        print("Did not reject on interim " + str(k+1))
if test.decision == "accept":
    print("Accepted the null")
import matplotlib.pyplot as plt

plt.boxplot([X, Y])
plt.show()
