# pyGST

Small library of group sequential testing in python

## Installation

To install the dependencies, do:
```
pip install git+https://github.com/rlberry-py/rlberry.git
pip install -r requirements.txt
```

The boundaries for sequential testing are computed using the R library `ldbounds`. R should be installed to be able to use the scripts, the library should install automatically.

Once this is done, the code can be used from the root directory of pyGST.

## Simulations & results

The file `experiment.py` give an example of how to do a simulation and experiment and then dump the results into the `outputs` direcctory and the notebook `Illustration.ipynb` give an example of how to visualize the results.

## Remark: power of the tests
For now, we don't implement any control on the test's power. In the case of Pocock and Obrien tests however, one can find the proper sample size
to attain a given power in the book "Group Sequential Methods with Applications to Clinical Trials".
