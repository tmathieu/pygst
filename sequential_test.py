import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy import stats
from tqdm import tqdm
from statsmodels.stats.power import tt_ind_solve_power as power_tt
from rlberry.agents import Agent
from rlberry.envs.bandits import Bandit
import rlberry.spaces as spaces
from rlberry.envs.interface import Model
from tqdm import tqdm

import logging
from pathlib import Path
import os

import itertools

logger = logging.getLogger(__name__)


class DummyEnv(Model):
    def __init__(self, n_arms, n, **kwargs):
        Model.__init__(self, **kwargs)
        self.n_arms = n_arms
        self.action_space = spaces.Discrete(self.n_arms)
        self.n = n

    def step(self, action):
        pass

    def reset(self):
        return 0


class GST(Agent):
    """
    Base class for Group Sequential testing.
    """

    name = "GST"

    def __init__(self, env, K, alpha=0.05, **kwargs):

        Agent.__init__(self, env, **kwargs)

        self.n_arms = self.env.action_space.n
        self.arms = np.arange(self.n_arms)
        self.total_time = 0

        self.K = K  # number of groups, or maximum number of tests
        self.alpha = alpha  # level of the test
        self.decision = "accept"
        self.n = self.env.n

    def eval(self, **kwargs):
        pass


class GGST(GST):
    """
    Group-Sequential two sample bilateral tests. Data are supposed Gaussian with equal variance.
    With K equally sized groups. Level should be

    Parameters
    ----------

    env: rlberry environment or None
        environment constructed using the class Generator from sequential_test.py.
        The environment contains the laws of the sample and the size of a block.
        If None, observations must be provided at each step and partial fit must be used.
    K: int
        maximum number of interims. If None, use pow to infer it.
    n: int
        size of a block. Used if env is None.
    alpha: float, default=0.05
        level of the test
    power: float, default=0.9
        If K is None, power of the test.
    name: str, default="PK"
        name of the boundary used. "PK" for Pocock, "OF" for O'Brien-Fleming, numerical value
        use the lan-demetz level-spending functions.
        name values of 1 and 2 correspond to alpha spending functions which give O’Brien Fleming and
        Pocock type boundaries, respectively. A value of 3 is the power family. Here, the spending function
        is αtφ, where φ must be greater than 0. A value of 4 is the Hwang-Shih-DeCani family, with
        spending function α(1 − e−φt)/(1 − e−φ), where φ cannot be 0.
    sigma: float, default=1
        std of the samples. Unused if student_approx = True
    student_approx: bool, default=False
        if True, use studentized Z-score and student ppf, this is a a heuristic and may have level different from alpha

    """

    name = "GGST"

    def __init__(
        self,
        env=None,
        K=None,
        n=10,
        alpha=0.05,
        power=0.9,
        name="PK",
        sigma=1,
        student_approx=False,
        boundary=None,
        **kwargs
    ):
        if env is None:
            env = DummyEnv(2, n)

        self.alpha = alpha
        if n is not None:
            self.n = n
        else:
            self.n = env[1]["n"]
        self.bname = name

        if K is None:
            K_ = self._find_K_power(power)
        else:
            K_ = K
        self.K = K_
        GST.__init__(self, env, K_, alpha, **kwargs)

        self.decision = "accept"
        self.student_approx = student_approx
        self.sigma = sigma  # Only used if student_approx == False
        self.boundary_computed = False
        if boundary is None:
            self.boundary = self.get_boundary(alpha, K_)
            self.boundary = self.boundary.sort_values("t")["up"].values
        else:
            # assumed to be a precomputed self.boundary
            self.boundary = boundary
        self.k = 0

    def _find_K_power(self, power):
        # Find K such that the power is at least `power` at a drift of one time the std.
        df = pd.read_csv("power_dataframe.csv")
        df = df.loc[df["name"] == self.bname]
        df = df.loc[np.abs(df["alpha"] - self.alpha) < 1e-5]
        df = df.loc[df["n"] == self.n]
        df = df.sort_values(by="K")
        Ks = df["K"].values
        print(df)
        powers = df["power"].values
        return Ks[np.min(np.where(powers > power))]

    def get_boundary(self, alpha, K):
        df = pd.read_csv("boundaries.csv", sep=";")
        df = df.loc[df["name"] == self.bname]
        df = df.loc[df["K"] == K]
        df = df.loc[np.abs(df["alpha"] - alpha) < 1e-5]  # allow for small approximation
        if len(df) == 0:
            if self.boundary_computed:
                raise RuntimeError("Problem with the boundary computation.")
            os.system(
                "Rscript boundary.R -n "
                + str(self.bname)
                + " -a "
                + str(alpha)
                + " -l "
                + str(K)
            )
            logger.info("Boundary computed")
            logger.info(
                "Using the command "
                + "Rscript boundary.R -n "
                + str(self.bname)
                + " -a "
                + str(alpha)
                + " -l "
                + str(K)
            )
            self.boundary_computed = True
            self.get_boundary(alpha, K)
        else:
            self.boundary = df
        return df

    def partial_fit(self, X, Y):
        k = self.k
        if not self.student_approx:
            Zk = np.sum(X - Y) / np.sqrt(2 * len(X) * self.sigma**2)
        else:
            Zk = np.sum(X - Y) / np.sqrt(
                len(X) * (np.std(X, ddof=1) ** 2 + np.std(Y, ddof=1) ** 2)
            )

        dof = 2 * len(X) - 2
        ck = self.get_ck(k)

        if not self.student_approx:
            threshold = ck
        else:
            threshold = -stats.t.ppf(q=1 - stats.norm.cdf(ck), df=dof)

        self.n_iter = (k + 1) * self.n
        if np.abs(Zk) > threshold:
            self.decision = "reject"
        self.k += 1

    def fit(self, budget=None):
        """
        Do the experiment

        Parameters
        ----------

        budget: int
            unused, defined for compatibility with rlberry.
        """

        X = np.array([])
        Y = np.array([])

        for k in range(self.K):
            _, X_g, _, _ = self.env.step(0)  # sample n samples from law 1
            _, Y_g, _, _ = self.env.step(1)  # sample n samples from law 2
            X = np.append(X_g, X)
            Y = np.append(Y_g, Y)

            self.partial_fit(X, Y)
            if self.decision == "reject":
                break

        self.writer.add_scalar("decision", self.decision, 0)
        self.writer.add_scalar("n_iter", self.n_iter, 0)

        info = {}
        return info

    def get_ck(self, k):
        return self.boundary[k]

    def draw_region(self, ax=None):
        """
        Plot the rejection region into the axis ax. If ax is None, create an axis.
        """
        assert not self.student_approx, "region for student is not defined properly"

        x = np.arange(1, self.K + 1) / self.K
        y = self.boundary

        if ax is None:
            fig, ax = plt.subplots()

        if not ax.lines:
            p1 = ax.plot(
                x,
                stats.norm.ppf(self.alpha / 2) * np.ones(self.K),
                "--",
                label="Non seq test",
                alpha=0.7,
            )
            ax.plot(
                x,
                -stats.norm.ppf(self.alpha / 2) * np.ones(self.K),
                "--",
                color=p1[0].get_color(),
                alpha=0.7,
            )

        p2 = ax.plot(x, y, "o-", label="Seq test " + self.bname, alpha=0.7)
        ax.plot(x, -np.array(y), "o-", color=p2[0].get_color(), alpha=0.7)

        plt.legend()
        plt.xlabel("portion of sample size")
        plt.ylabel("Z-stat")
        plt.title("Renormalized thresholds, unknown power")
        return y


class Generator(Bandit):
    """
    Generator environment.

    Parameters
    ----------

    laws: list of scipy laws
        laws of the arms. Must support the method law.rvs and which should accept random state and size parameter.

    n: int, default=5
        size of the samples to be drawn. Correspond to the size of a group in GST.

    """

    def __init__(self, laws=[], n=5, **kwargs):
        Bandit.__init__(self, **kwargs)
        self.laws = laws
        self.n_arms = len(self.laws)
        self.n = n  # group size
        self.action_space = spaces.Discrete(self.n_arms)

    def step(self, action):
        reward = self.laws[action].rvs(random_state=self.rng, size=self.n)
        return 0, reward, True, {}

    def reset(self):
        return 0


class PTGGST(GST):
    """
    Permutation Group-Sequential test.
    With K equally sized groups. Level should be approximately alpha = 5%.
    """

    name = "PTGGST"

    def __init__(
        self,
        env,
        K,
        Kc,
        alpha=0.05,
        name="PK",
        student_approx=False,
        sigma=1,
        B=None,
        stat_fun="mean",
        **kwargs
    ):

        GST.__init__(self, env, K, alpha, **kwargs)

        self.decision = "accept"
        self.n_iter = K * self.env.n
        self.B = B  # number of samples to approximate permutation stat. If None, use exact test.
        self.Kc = Kc
        self.stat_fun = stat_fun  # statistic of interest

        self.ggst = GGST(
            None,
            K - Kc,
            alpha=alpha * (1 - Kc / K),
            n=self.env.n,
            name=name,
            student_approx=student_approx,
            sigma=sigma,
        )

    def comparison_fun(self, data, perm=None):
        if self.stat_fun == "mean":
            stat_fun = lambda x: np.mean(x)

        if perm is None:
            perm = np.arange(len(data))
        data = data[perm]
        return np.abs(
            stat_fun(data[: (len(data) // 2)]) - stat_fun(data[(len(data) // 2) :])
        )

    def fit(self, sampler):

        levels = 1 / self.K * self.alpha * np.ones(self.K)

        X = np.array([])
        Y = np.array([])
        rng = np.random.RandomState()

        for k in range(self.K):
            _, X_g, _, _ = self.env.step(0)
            _, Y_g, _, _ = self.env.step(1)
            X = np.append(X_g, X)
            Y = np.append(Y_g, Y)

            if k < self.Kc:
                all_data = np.hstack([X, Y])

                t0 = self.comparison_fun(all_data)

                level = levels[k]
                if self.B is not None:
                    Ts = [
                        self.comparison_fun(
                            all_data, self.rng.permutation(len(all_data))
                        )
                        for _ in range(self.B)
                    ]
                else:
                    combs = itertools.combinations(
                        np.arange(len(all_data)), len(all_data) // 2
                    )
                    idxs = np.arange(len(all_data))
                    Ts = [
                        self.comparison_fun(
                            all_data, perm=np.hstack([choice, np.delete(idxs, choice)])
                        )
                        for choice in combs
                    ]

                pval = np.mean(Ts > t0)

                if pval < level:
                    self.n_iter = (k + 1) * self.n
                    self.decision = "reject"
                    break
            else:
                self.ggst.partial_fit(X, Y)
                if self.ggst.decision == "reject":
                    self.n_iter = (k + 1) * self.n
                    self.decision = "reject"
                    break

        info = {"decision": self.decision, "n_iter": self.n_iter}
        self.writer.add_scalar("decision", self.decision, 0)
        self.writer.add_scalar("n_iter", self.n_iter, 0)

        return info


from scipy.stats import wilcoxon


class UBTGST(GST):
    """
    Union Bound Group-Sequential test, student test.
    With K equally sized groups. Type I error should be smaller than alpha = 5%.
    """

    name = "UBTGST"

    def __init__(
        self, env, K=None, alpha=0.05, power=0.9, sigma=1, stat_fun="mean", **kwargs
    ):

        GST.__init__(self, env, K, alpha, **kwargs)

        self.decision = "accept"
        self.power = power
        if K is None:
            K_ = self._find_K(power)
        else:
            K_ = K
        self.K = K_
        self.n_iter = K_ * self.env.n

    def _find_K(self, power):
        # Find K for a power of power and drift of 1 std, using only last step. Very naive.
        K = 1
        dof = 2 * self.env.n - 2
        level = 1 / K * self.alpha
        p = power_tt(1, self.env.n, self.alpha)
        while p < power:
            K += 1
            p = power_tt(1, K*self.env.n, self.alpha/K)
        print(K)
        return K

    def fit(self, sampler):

        level = 1 / self.K * self.alpha

        X = np.array([])
        Y = np.array([])
        rng = np.random.RandomState()

        for k in range(self.K):
            _, X_g, _, _ = self.env.step(0)
            _, Y_g, _, _ = self.env.step(1)
            X = np.append(X_g, X)
            Y = np.append(Y_g, Y)

            Zk = np.sum(X - Y) / np.sqrt(
                len(X) * (np.std(X, ddof=1) ** 2 + np.std(Y, ddof=1) ** 2)
            )

            dof = 2 * len(X) - 2
            threshold = stats.t.ppf(q=1 - level / 2, df=dof)

            if np.abs(Zk) > threshold:
                self.n_iter = (k + 1) * self.n
                self.decision = "reject"
                break

        info = {"decision": self.decision, "n_iter": self.n_iter}
        self.writer.add_scalar("decision", self.decision, 0)
        self.writer.add_scalar("n_iter", self.n_iter, 0)

        return info


class Mixture:
    """
    Class for mixture of gaussians.

    Parameters
    ----------

    means: array

    stds: array

    weight: float
       weight of first gaussian

    """

    def __init__(self, means=np.array([0, 2]), stds=np.array([1, 1]), weight=0.5):
        self.means = means
        self.stds = stds
        self.weight = weight

    def rvs(self, random_state, size=1):
        bernoullis = random_state.binomial(1, self.weight, size=size)
        X1 = stats.norm(loc=self.means[0], scale=self.stds[0]).rvs(
            size=size, random_state=random_state
        )
        X2 = stats.norm(loc=self.means[1], scale=self.stds[1]).rvs(
            size=size, random_state=random_state
        )
        return X1 * bernoullis + X2 * (1 - bernoullis)

    def mean(self):
        return np.sum(self.weights * self.means)
